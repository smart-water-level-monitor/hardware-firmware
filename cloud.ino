BLYNK_CONNECTED()
{
  Serial.println("[INFO] Device Connected to Blynk Cloud");
  Blynk.logEvent("online", DEVICE_NAME + " connected to internet");
  Blynk.syncAll();
}

BLYNK_WRITE(V0)
{
  int status = param.asInt();
  Serial.printf("[INFO] Status of OnOff Button: %d\n",status);
}

BLYNK_WRITE(V3)
{
  String terminalInput = param.asStr();

  if (terminalInput == "clear")
    terminal.clear();
  if (terminalInput == "wipe")
  {
    wipeEeprom(0, 208);
    terminal.printf("[INFO] EEPROM Wiped Successfully");
  }
  if (terminalInput == "restart")
  {
    ESP.reset();
    terminal.printf("[INFO] Device Restarted Successfully");
  }

  terminal.flush();
}

void updateWaterLevelWidget(void)
{
  // Multiple readings to get accurate data
  currentDepth = getWaterLevel(DEPTH_SENSOR_TRANSMITTER_PIN, DEPTH_SENSOR_RECEIVER_PIN);
  currentDepth = getWaterLevel(DEPTH_SENSOR_TRANSMITTER_PIN, DEPTH_SENSOR_RECEIVER_PIN);

  Blynk.virtualWrite(V1, currentDepth);
  Blynk.virtualWrite(V2, String(currentDepth));
}